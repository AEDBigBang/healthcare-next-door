///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package Business.Insurance;
//
///**
// *
// * @author ankit
// */
//public class InsurancePolicy {
//
//    private String policyName;
//    private int policyCoverage;
//    private String policyTC;
//
//    public InsurancePolicy(String policyName, int policyCoverage, String policyTC) {
//        this.policyName = policyName;
//        this.policyCoverage = policyCoverage;
//        this.policyTC = policyTC;
//    }
//
//    public String getPolicyName() {
//        return policyName;
//    }
//
//    public void setPolicyName(String policyName) {
//        this.policyName = policyName;
//    }
//
//    public int getPolicyCoverage() {
//        return policyCoverage;
//    }
//
//    public void setPolicyCoverage(int policyCoverage) {
//        this.policyCoverage = policyCoverage;
//    }
//
//    public String getPolicyTC() {
//        return policyTC;
//    }
//
//    public void setPolicyTC(String policyTC) {
//        this.policyTC = policyTC;
//    }
//
//    @Override
//    public String toString() {
//        return policyName;
//    }
//
//}
